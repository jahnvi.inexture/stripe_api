import React from "react";
import Product from "./components/Product";

function App() {
  return (
    <div className="App">
      <Product />
      {/* <header className="App-header">
        <h1>Make Stripe Payment</h1>
        <p>Pay Total of $ {totalPrice}</p>
        <p>
          <StripeCheckoutButton price={totalPrice} />
        </p>
      </header> */}
    </div>
  );
}

export default App;
