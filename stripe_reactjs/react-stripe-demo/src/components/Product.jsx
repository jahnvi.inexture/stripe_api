import { Card } from "react-bootstrap";
import StripeCheckoutButton from "./stripe-button";

const Product = () => {
  const totalPrice = 999;

  return (
    <Card style={{ width: "18rem", left: "37rem" }}>
      <Card.Img
        variant="top"
        src="https://images.pexels.com/photos/18105/pexels-photo.jpg?auto=compress"
      />
      <Card.Body>
        <Card.Title>Apple MacBook Pro</Card.Title>
        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
        <Card.Title variant="primary">$999</Card.Title>
        <StripeCheckoutButton price={totalPrice} />
      </Card.Body>
    </Card>
  );
};

export default Product;
