const bodyParser = require("body-parser");
const express = require("express");
const path = require("path");

const PUBLISHABLE_KEY =
  "pk_test_51Jh6OnSFX2yixa3287WzJxrXHDJAqGSXoMGdi29h9k9ADNFkfiRPOUNNtQNVKRyFMbIwaLil2hxI3hcYldy3KJ8H00Bb8Y541x";
const SECRET_KEY =
  "sk_test_51Jh6OnSFX2yixa32k2wiYOIz2y64ITBdjb6zDP6KGh3uGZqgoHChjL9d1O78E0JW8SFjIl62F8C2O2d4UeReFpqv00ZlzuCzwb";

const stripe = require("stripe")(SECRET_KEY);

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const PORT = process.env.PORT || 5000;
app.set("view engine", "ejs");

app.get("/", (req, res) => {
  res.render("Home", {
    key: PUBLISHABLE_KEY,
  });
});

app.post("/payment", (req, res) => {
  stripe.customers
    .create({
      email: req.body.stripeEmail,
      source: req.body.stripeToken,
      name: "Gautam Sharma",
      address: {
        line1: "3386 Bell Street",
        postal_code: "10007",
        city: "New York",
        state: "NY",
        country: "US",
      },
    })
    .then((customer) => {
      return stripe.charges.create({
        amount: 7000, // Charing Rs 25
        description: "Web Development Product",
        currency: "USD",
        customer: customer.id,
      });
    })
    .then((charge) => {
      res.send("Success"); // If no error occurs
    })
    .catch((err) => {
      res.send(err); // If some error occurs
    });
});

app.listen(PORT, () => {
  console.log(`App is listing on ${PORT}`);
});
