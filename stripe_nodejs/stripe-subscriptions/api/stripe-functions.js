/* Server Side -- Stripe API calls */
require("dotenv").config();
const stripe = require("stripe")(process.env.STRIPE_API_KEY);
const UTILS = require("../utils/format-numbers.js");

function getAllProductsAndPlans() {
  return Promise.all([stripe.products.list({}), stripe.plans.list({})]).then(
    (stripeData) => {
      var products = stripeData[0].data;
      var plans = stripeData[1].data;

      plans = plans
        .sort((a, b) => {
          return a.amount - b.amount;
        })
        .map((plan) => {
          /* Format plan price (amount) */
          amount = UTILS.formatUSD(plan.amount);
          return { ...plan, amount };
        });

      products.forEach((product) => {
        const filteredPlans = plans.filter((plan) => {
          return plan.product === product.id;
        });

        product.plans = filteredPlans;
      });

      return products;
    }
  );
}

function createCustomerAndSubscription(requestBody) {
  return stripe.customers
    .create({
      source: requestBody.stripeToken,
      email: requestBody.customerEmail,
    })
    .then((customer) => {
      stripe.subscriptions.create({
        customer: customer.id,
        items: [
          {
            plan: requestBody.planId,
          },
        ],
      });
    });
}

module.exports = {
  getAllProductsAndPlans,
  createCustomerAndSubscription,
};
